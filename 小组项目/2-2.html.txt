﻿
<html>
<head>
<title>假日旅游</title>
<style type="text/css">
body{background-color:#7FFFD4}
.p1{color:red;font-size:150%;font-family:KaiTi;font-weight:bold}
.p2{color:pink;font-size:150%;font-family:FangSong}
.p3{color:yellow;font-size:150%;font-family:SimSun}
.p4{color:blue;font-size:150%;font-family:KaiTi;font-weight:normal}
.p5{color:green;font-size:150%;font-family:FangSong}
.p6{color:pink;font-size:150%;font-family:KaiTi}
</style>
</head>
<body>
<p class='p1'>项目介绍:我们的网站致力于闲情假日人们出游的地方，给人们介绍不一样的旅游，不一样的景点。我们的网站可以让你认识具有相同爱好的人，我们可以组织相同爱好旅游的人一起去旅游</p>
<p class='p2'>市场需求:在现代化建设中，尤其是旅游期间，人们最喜爱最常浏览的网页，人们在迷茫的时候，都可以阅览此网站。最后会有更多的阅览，了解此网站</p>
<p class='p3'>网站功能:此网站可以查询旅游地形图，可以告知旅游胜地，让人们更清楚的了解到旅游风景的优势，可以组织相同爱好的人，一同前去旅游</p>
<p class='p4'>商业模式:在生活中，尤其是互联网中可以看出其商业价值，看出此网站对人们有没有用，由山东商业职业技术学院计算机应用班创建</p>
<p class='p5'>团队分工:队长负责网站的管理，副队长负责人们的浏览及帮助人们介绍</p>
<p class='p6'>财务预估:每天会有20人浏览，有50以内的人提出相关问题，我们都会解决</p>
</body>
</html>
